import vk_api
import time
from bot_music import music_msg

#for group
vk = vk_api.VkApi(token='1372ae1307df2b06b1f5ceebe176e0b022f44379bf15d3e8ee524e649e4f9fee7cdd4ed727d214ef31bd0')
vk._auth_token()

values = {"offset": 0,
          "count": 20,
          "filter": 'unread'}
vk.method('messages.getConversations', values)


def write_msg(user_id, msg):
    vk.method('messages.send', {
        'user_id': user_id,
        'message': msg})


while True:
    response = vk.method('messages.getConversations', values)
    music_response = vk.method('audio.get', 'club170378440')
    items = response['items']
    if items:
        for i in items:
            last_message = i['last_message']['text']
            print(last_message)
            if last_message == 'Music':
                music_msg(i['last_message']['from_id'],
                          'music for you.')
            else:
                write_msg(i['last_message']['from_id'],
                          i['last_message']['text'])
    time.sleep(1)

